# php-extended/php-data-finder-array

An implementation of the data finder object based on a memory based php array.

![coverage](https://gitlab.com/php-extended/php-data-finder-array/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-data-finder-array/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-data-finder-array ^8`


## Basic Usage

This library is designed to be used with a collection of data packed as a php
array. The data should be on object form and given to the data finder.

The data finder provides query methods to retrieve specific objects that have
specific properties within the list, without having to implement the search,
ordering, offset and limit mecanisms over and over again.

For an example usage, see the test case for the `ArrayDataFinder` class.


## License

MIT (See [license file](LICENSE)).
