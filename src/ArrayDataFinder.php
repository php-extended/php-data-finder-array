<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-finder-array library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DataFinder;

use ArrayIterator;
use Iterator;

/**
 * ArrayDataFinder class file.
 * 
 * This class represents a data finder that searches from data into an internal
 * memory based php array.
 * 
 * @author Anastaszor
 * @template T of object
 * @implements DataFinderInterface<T>
 */
class ArrayDataFinder implements DataFinderInterface
{
	
	/**
	 * The data objects.
	 * 
	 * @var array<T>
	 */
	protected array $_data = [];
	
	/**
	 * The name of the field or method to call on the objects to compare for
	 * their identifier value.
	 * 
	 * @var string
	 */
	protected string $_identifierFieldName;
	
	/**
	 * Builds a new ArrayDataFinder with the given array of data and the name
	 * of the field that serves as primary key.
	 * 
	 * @param array<T> $data
	 * @param string $identifierFieldName
	 */
	public function __construct(array $data, string $identifierFieldName)
	{
		$this->_data = $data;
		$this->_identifierFieldName = $identifierFieldName;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataFinder\DataFinderInterface::findById()
	 */
	public function findById(string $identifier) : ?object
	{
		foreach($this->_data as $object)
		{
			if($this->callMatch($object, $this->_identifierFieldName, $identifier))
			{
				return $object;
			}
		}
		
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataFinder\DataFinderInterface::findFirstMatch()
	 */
	public function findFirstMatch(array $attributeValues, array $orderings = [], int $offset = 0, int $limit = 10) : ?object
	{
		foreach($this->findMatches($attributeValues, $orderings, $offset, $limit) as $object)
		{
			return $object;
		}
		
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataFinder\DataFinderInterface::findMatches()
	 */
	public function findMatches(array $attributeValues, array $orderings = [], int $offset = 0, int $limit = 10) : Iterator
	{
		$currentOffset = 0;
		$results = [];
		
		foreach($this->_data as $object)
		{
			foreach($attributeValues as $attributeName => $attributeValue)
			{
				if(!$this->callMatch($object, $attributeName, $attributeValue))
				{
					continue 2;
				}
			}
			
			// object qualifies, we have to order it into the current results
			// array. The results array should not contain more objects than
			// the limit
			
			$results[] = $object;
			\usort(
				$results, 
			/**
			 * @param object $object1
			 * @param object $object2
			 */
			function($object1, $object2) use ($orderings)
			{
				foreach($orderings as $fieldName => $direction)
				{
					$result = $this->callCompare($object1, $object2, $fieldName, $direction);
					if(0 !== $result)
					{
						return $result;
					}
				}
				
				return 0;
			},
			);
			
			// now that the found object is in the array, eject an object based
			// on the limits
			
			if(\count($results) > $limit)
			{
				if($currentOffset < $offset)
				{
					// eject the first object of the array
					unset($results[0]);
					$currentOffset++;
					continue;
				}
				// eject the last object of the array
				unset($results[\count($results) - 1]);
			}
		}
		
		while($currentOffset < $offset)
		{
			$results = \array_values($results);
			unset($results[0]);
			$currentOffset++;
		}
		
		return new ArrayIterator(\array_values($results));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataFinder\DataFinderInterface::findFirstLike()
	 */
	public function findFirstLike(array $attributeValues, array $orderings = [], int $offset = 0, int $limit = 10) : ?object
	{
		foreach($this->findLike($attributeValues, $orderings, $offset, $limit) as $object)
		{
			return $object;
		}
		
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataFinder\DataFinderInterface::findLike()
	 */
	public function findLike(array $attributeValues, array $orderings = [], int $offset = 0, int $limit = 10) : Iterator
	{
		$currentOffset = 0;
		$results = [];
		
		foreach($this->_data as $object)
		{
			foreach($attributeValues as $attributeName => $attributeValue)
			{
				if(!$this->callLike($object, $attributeName, $attributeValue))
				{
					continue 2;
				}
			}
			
			// object qualifies, we have to order it into the current results
			// array. The results array should not contain more objects than
			// the limit
			
			$results[] = $object;
			\usort(
				$results, 
			/**
			 * @param object $object1
			 * @param object $object2
			 */
			function($object1, $object2) use ($orderings)
			{
				foreach($orderings as $fieldName => $direction)
				{
					$result = $this->callCompare($object1, $object2, $fieldName, $direction);
					if(0 !== $result)
					{
						return $result;
					}
				}
				
				return 0;
			},
			);
			
			// now that the found object is in the array, eject an object based
			// on the limits
			
			if(\count($results) > $limit)
			{
				if($currentOffset < $offset)
				{
					// eject the first object of the array
					unset($results[0]);
					$currentOffset++;
					continue;
				}
				// eject the last object of the array
				unset($results[\count($results) - 1]);
			}
		}
		
		while($currentOffset < $offset)
		{
			$results = \array_values($results);
			unset($results[0]);
			$currentOffset++;
		}
		
		return new ArrayIterator(\array_values($results));
	}
	
	/**
	 * Gets whether the given object has the given value for the given field
	 * name.
	 * 
	 * @param object $object
	 * @param string $fieldName
	 * @param string $fieldValue
	 * @return boolean
	 */
	protected function callMatch($object, string $fieldName, string $fieldValue) : bool
	{
		return $this->getValue($object, $fieldName) === $fieldValue;
	}
	
	/**
	 * Gets whether the given object has the given field name value that is
	 * like the given value.
	 * 
	 * @param object $object
	 * @param string $fieldName
	 * @param string $fieldValue
	 * @return boolean
	 */
	protected function callLike($object, string $fieldName, string $fieldValue) : bool
	{
		return $this->doLike($fieldValue, (string) $this->getValue($object, $fieldName));
	}
	
	/**
	 * Gets whether the given value matches (in the sense of SQL LIKE) the
	 * given pattern.
	 * 
	 * @param string $pattern
	 * @param string $value
	 * @return boolean
	 */
	protected function doLike(string $pattern, string $value) : bool
	{
		$pattern = \str_replace('%', '.*', \str_replace('_', '.', \preg_quote($pattern, '|')));
		
		return (bool) \preg_match('|^'.$pattern.'$|', $value);
	}
	
	/**
	 * Gets the comparison result between two values.
	 * 
	 * @param object $object1
	 * @param object $object2
	 * @param string $fieldName
	 * @param boolean $direction
	 * @return integer
	 */
	protected function callCompare($object1, $object2, string $fieldName, bool $direction) : int
	{
		$value1 = $this->getValue($object1, $fieldName);
		$value2 = $this->getValue($object2, $fieldName);
		
		return ($direction ? -1 : 1) * \strcmp((string) $value1, (string) $value2);
	}
	
	/**
	 * Gets the value of the object for the given field name.
	 * 
	 * @param object $object
	 * @param string $fieldName
	 * @return null|boolean|integer|float|string
	 * @psalm-suppress MixedInferredReturnType
	 */
	protected function getValue($object, string $fieldName)
	{
		$return = null;
		
		if(\property_exists($object, $fieldName))
		{
			/** @psalm-suppress MixedReturnStatement */
			return $object->{$fieldName};
		}
		
		$getter = 'get'.\ucfirst($fieldName);
		if(\method_exists($object, $getter))
		{
			/** @psalm-suppress MixedReturnStatement */
			return $object->{$getter}();
		}
		
		$isser = 'is'.\ucfirst($fieldName);
		if(\method_exists($object, $isser))
		{
			/** @psalm-suppress MixedReturnStatement */
			return $object->{$isser}();
		}
		
		$hasser = 'has'.\ucfirst($fieldName);
		if(\method_exists($object, $hasser))
		{
			/** @psalm-suppress MixedReturnStatement */
			return $object->{$hasser}();
		}
		
		return $return;
	}
	
}
