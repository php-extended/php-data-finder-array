<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-finder-array library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DataFinder\ArrayDataFinder;
use PhpExtended\DataFinder\DataFinderInterface;
use PHPUnit\Framework\TestCase;

class DataObject
{	
	
	public string $id;
	public string $name;
	public string $data;
	
	public function __construct(string $id, string $name, string $data)
	{
		$this->id = $id;
		$this->name = $name;
		$this->data = $data;
	}
	
	public function getFullName() : string
	{
		return $this->name;
	}
	
	public function isDatum() : string
	{
		return $this->data;
	}
	
	public function hasFullDatum() : string
	{
		return $this->data;
	}
	
}


/**
 * ArrayDataFinderTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DataFinder\ArrayDataFinder
 *
 * @internal
 *
 * @small
 */
class ArrayDataFinderTest extends TestCase
{	
	/**
	 * The object to test.
	 * 
	 * @var ArrayDataFinder
	 */
	protected ArrayDataFinder $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testFindByIdFound() : void
	{
		$this->assertEquals(new DataObject('id01', 'horse', 'staple'), $this->_object->findById('id01'));
	}
	
	public function testFindByIdNotFound() : void
	{
		$this->assertNull($this->_object->findById('fk01'));
	}
	
	public function testFindByMatchFound() : void
	{
		$this->assertEquals(new ArrayIterator([new DataObject('id04', 'catty', 'ocean')]), $this->_object->findMatches(['name' => 'catty']));
	}
	
	public function testFindByMatchNotFound() : void
	{
		$this->assertEquals(new ArrayIterator(), $this->_object->findMatches(['name' => 'piggy']));
	}
	
	public function testFindFirstByMatchFound() : void
	{
		$this->assertEquals(new DataObject('id05', 'doggy', 'scarlett'), $this->_object->findFirstMatch(['name' => 'doggy']));
	}
	
	public function testFindFirstByMatchNotFound() : void
	{
		$this->assertNull($this->_object->findFirstMatch(['name' => 'piggy']));
	}
	
	public function testFindAllByMatchFound() : void
	{
		$expected = new ArrayIterator([
			new DataObject('id05', 'doggy', 'scarlett'),
			new DataObject('id06', 'duckky', 'scarlett'),
		]);
		$this->assertEquals($expected, $this->_object->findMatches(['data' => 'scarlett'], ['value' => DataFinderInterface::ORDER_ASC]));
	}
	
	public function testFindAllByMatchWithOrder() : void
	{
		$expected = new ArrayIterator([
			new DataObject('id05', 'doggy', 'scarlett'),
			new DataObject('id06', 'duckky', 'scarlett'),
		]);
		$this->assertEquals($expected, $this->_object->findMatches(['data' => 'scarlett'], ['name' => DataFinderInterface::ORDER_ASC]));
	}
	
	public function testFindAllByMatchWithOffset() : void
	{
		$expected = new ArrayIterator([
			new DataObject('id06', 'duckky', 'scarlett'),
		]);
		$this->assertEquals($expected, $this->_object->findMatches(['data' => 'scarlett'], ['name' => DataFinderInterface::ORDER_ASC], 1, 1));
	}
	
	public function testFindAllByMatchWithOffsetLargeLimit() : void
	{
		$expected = new ArrayIterator([
			new DataObject('id06', 'duckky', 'scarlett'),
		]);
		$this->assertEquals($expected, $this->_object->findMatches(['data' => 'scarlett'], ['name' => DataFinderInterface::ORDER_ASC], 1, 10));
	}
	
	public function testFindAllByMatchWithLimit() : void
	{
		$expected = new ArrayIterator([
			new DataObject('id05', 'doggy', 'scarlett'),
		]);
		$this->assertEquals($expected, $this->_object->findMatches(['data' => 'scarlett'], ['name' => DataFinderInterface::ORDER_ASC], 0, 1));
	}
	
	public function testFindByLikeFound() : void
	{
		$this->assertEquals(new ArrayIterator([new DataObject('id04', 'catty', 'ocean')]), $this->_object->findLike(['name' => 'catty']));
	}
	
	public function testFindByLikeNotFound() : void
	{
		$this->assertEquals(new ArrayIterator(), $this->_object->findLike(['name' => 'piggy']));
	}
	
	public function testFindFirstByLikeFound() : void
	{
		$this->assertEquals(new DataObject('id05', 'doggy', 'scarlett'), $this->_object->findFirstLike(['name' => 'doggy']));
	}
	
	public function testFindFirstByLikeNotFound() : void
	{
		$this->assertNull($this->_object->findFirstLike(['name' => 'piggy']));
	}
	
	public function testFindAllByLikeFound() : void
	{
		$expected = new ArrayIterator([
			new DataObject('id05', 'doggy', 'scarlett'),
			new DataObject('id06', 'duckky', 'scarlett'),
		]);
		$this->assertEquals($expected, $this->_object->findLike(['data' => 'scarl%'], ['value' => DataFinderInterface::ORDER_ASC]));
	}
	
	public function testFindAllByLikeWithOrder() : void
	{
		$expected = new ArrayIterator([
			new DataObject('id05', 'doggy', 'scarlett'),
			new DataObject('id06', 'duckky', 'scarlett'),
		]);
		$this->assertEquals($expected, $this->_object->findLike(['data' => 'scarl%'], ['name' => DataFinderInterface::ORDER_ASC]));
	}
	
	public function testFindAllByLikeWithOffset() : void
	{
		$expected = new ArrayIterator([
			new DataObject('id06', 'duckky', 'scarlett'),
		]);
		$this->assertEquals($expected, $this->_object->findLike(['data' => 'scarl%'], ['name' => DataFinderInterface::ORDER_ASC], 1, 1));
	}
	
	public function testFindAllByLikeWithOffsetLargeLimit() : void
	{
		$expected = new ArrayIterator([
			new DataObject('id06', 'duckky', 'scarlett'),
		]);
		$this->assertEquals($expected, $this->_object->findLike(['data' => 'scarl%'], ['name' => DataFinderInterface::ORDER_ASC], 1, 10));
	}
	
	public function testFindAllByLikeWithLimit() : void
	{
		$expected = new ArrayIterator([
			new DataObject('id05', 'doggy', 'scarlett'),
		]);
		$this->assertEquals($expected, $this->_object->findLike(['data' => 'scarl%'], ['name' => DataFinderInterface::ORDER_ASC], 0, 1));
	}
	
	public function testFindMatchGetter() : void
	{
		$this->assertEquals(new DataObject('id02', 'dog', 'battery'), $this->_object->findFirstMatch(['fullName' => 'dog']));
	}
	
	public function testFindMatchIsser() : void
	{
		$this->assertEquals(new DataObject('id02', 'dog', 'battery'), $this->_object->findFirstMatch(['datum' => 'battery']));
	}
	
	public function testFindMatchHasser() : void
	{
		$this->assertEquals(new DataObject('id02', 'dog', 'battery'), $this->_object->findFirstMatch(['fullDatum' => 'battery']));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ArrayDataFinder([
			new DataObject('id01', 'horse', 'staple'),
			new DataObject('id02', 'dog', 'battery'),
			new DataObject('id03', 'cat', 'correct'),
			new DataObject('id04', 'catty', 'ocean'),
			new DataObject('id05', 'doggy', 'scarlett'),
			new DataObject('id06', 'duckky', 'scarlett'),
		], 'id');
	}
	
}
